package com.gslab.client;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sun.jna.NativeLibrary;

import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.component.AudioMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;

public class Client {

	private static List<String> songsList;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean found = new NativeDiscovery().discover();
		System.out.println(found);
		System.out.println(LibVlc.INSTANCE.libvlc_get_version());
		File folder = null;
		if(args[0].equals("1")) {
			System.out.println("One");
			folder = new File("C:/Users/gs-0913/Downloads/Music");
		}else if(args[0].equals("2")){
			System.out.println("Two");
			folder = new File("C:/Users/gs-0913/Downloads/Music");
		}
		File[] listOfFiles = folder.listFiles();
		songsList = new ArrayList<String>();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				songsList.add(listOfFiles[i].getPath());
			}
		}
		
		Iterator iter = songsList.iterator();
		System.out.println(songsList.toString());
		while(iter.hasNext()) {
			//iter.next();
			AudioMediaPlayerComponent mediaPlayerComponent = new AudioMediaPlayerComponent();;
			Runnable runnable = new JukeBoxMediaPlayer(mediaPlayerComponent,iter.next().toString());
			Thread th = new Thread(runnable);
			th.start();
			long startTime = System.currentTimeMillis();
			long endTime = startTime;
			while(endTime<startTime+30000) {
				System.out.println("Thread Status : "+th.getState());
				if(!th.isAlive()) {
					break;
				}
				try {
					Thread.sleep(5000);
					endTime = endTime+5000;
					System.out.println("End Time : "+ (endTime-startTime)/1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(th.isAlive()){
				System.out.println("before stooped");
				 mediaPlayerComponent.getMediaPlayer().stop();
			}
		}
	

	}

}
