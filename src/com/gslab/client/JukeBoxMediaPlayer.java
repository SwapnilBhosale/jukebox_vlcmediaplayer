package com.gslab.client;

import uk.co.caprica.vlcj.component.AudioMediaPlayerComponent;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;

public class JukeBoxMediaPlayer implements Runnable {
	
	private AudioMediaPlayerComponent mediaPlayerComponent;
	private String songName;

	private MediaEventListner listener;
	private boolean isStopped = false;

	
	@Override
	public void run() {
		songCallBacks();
		while(!isStopped){
			System.out.println(".");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}

	private class MediaEventListner extends MediaPlayerEventAdapter{
		
		@Override	
		public void stopped(MediaPlayer mediaPlayer) {
			System.out.println("Stopped");
			isStopped= true;
		}
		@Override
		public void finished(MediaPlayer mediaPlayer) {
			System.out.println("finished");
			isStopped= true;
		}

		@Override
		public void error(MediaPlayer mediaPlayer) {
			//exit(1);
			System.out.println("error");
			isStopped= true;
		}
		
		@Override
		public void playing(MediaPlayer mediaPlayer) {
			System.out.println("playing");
		}
	}
	
	public JukeBoxMediaPlayer(AudioMediaPlayerComponent mediaPlayerComponent,
			String songName) {
		super();
		this.mediaPlayerComponent = mediaPlayerComponent;
		this.songName = songName;
		this.listener = new MediaEventListner();
	}


	public void songCallBacks() {
		mediaPlayerComponent.getMediaPlayer().playMedia(songName);
		mediaPlayerComponent.getMediaPlayer().addMediaPlayerEventListener(this.listener);
		
	}
	
}
